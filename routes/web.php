<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

Route::middleware('guest')->group(function () {
	Route::get('/login', function() {
        return redirect('/login/email');
    });
	Route::post('/login', [LoginController::class, 'login']);
	Route::get('/login/{type}', [LoginController::class, 'index'])->name('login');

	Route::get('/register', [RegisterController::class, 'index'])->name('registeration');
	Route::post('/register', [RegisterController::class, 'register']);
});
