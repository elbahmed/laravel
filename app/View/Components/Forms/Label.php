<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Label extends Component
{
    public $name;
    public $id;
    public $for;

    protected static $defaultName = "label";
    protected static $defaultId = "_label";
    protected static $defaultFor = "_input";



    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = null, $id = null, $for = null)
    {
        $this->_init();

        if ($name)
            $this->name = $name;

        if ($id)
            $this->id = $id;

        if ($for)
            $this->id = $for;


    }

    protected function _init()
    {
        $this->name = $this::$defaultName;
        $this->id = $this::$defaultId;
        $this->for = $this::$defaultFor;
    }


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.label');
    }
}
