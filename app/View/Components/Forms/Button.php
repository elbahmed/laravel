<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Button extends Component
{
    public $name;
    public $id;
    public $options;

    protected static $defaultName = "button";
    protected static $defaultId = "_button";
    protected static $defaultOptions = [
        'class' => [
            'btn',
            'btn-block',
            'btn-outline-primary',
        ],
        'style' => null,
    ];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = null, $id = null, $options = null)
    {
        $this->_init();

        if ($name)
            $this->name = $name;

        if ($id)
            $this->id = $id;

        if ($options) {

            foreach ($options as $key => $value){

                if (is_array($value) && $this->options[$key]) {
                    array_push($this->options[$key], $value);

                } else {
                    $this->options[$key] = $value;

                }
            }
        }

    }

    protected function _init()
    {
        $this->name = $this::$defaultName;
        $this->id = $this::$defaultId;
        $this->options = $this::$defaultOptions;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.button');
    }
}
