<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public $name;
    public $id;
    public $label;
    public $options;

    protected static $defaultName = "checkbox";
    protected static $defaultId = "_checkbox";
    protected static $defaultLabel = "checkbox";
    protected static $defaultOptions = [
        'class' => [
            'icheck-primary',
        ],
        'style' => null,
    ];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = null, $id = null, $label = null, $options = null)
    {
        $this->_init();

        if ($name)
            $this->name = $name;

        if ($id)
            $this->id = $id;

        if ($label)
            $this->label = $label;

        if ($options) {

            foreach ($options as $key => $value){

                if (is_array($value) && $this->options[$key]) {
                    array_push($this->options[$key], $value);

                } else {
                    $this->options[$key] = $value;

                }
            }
        }

    }

    protected function _init()
    {
        $this->name = $this::$defaultName;
        $this->id = $this::$defaultId;
        $this->label = $this::$defaultLabel;
        $this->options = $this::$defaultOptions;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.checkbox');
    }
}
