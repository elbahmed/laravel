<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Input extends Component
{
    public $name;
    public $id;
    public $label;
    public $options;

    protected static $defaultName = "input";
    protected static $defaultLabel = "input";
    protected static $defaultId = "_input";
    protected static $defaultOptions = [
        'placeholder' => null,
        'show_label' => false,
        'required' => false,
        'disabled' => false,
        'readonly' => false,
        'has_addon' => false,
        'type' => 'text',
        'addon' => null,
        'class' => [
            'form-control',
        ],
        'style' => null,

    ];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = null, $id = null, $label = null, $options = null)
    {
        $this->_init();

        if ($name)
            $this->name = $name;

        if ($id)
            $this->id = $id;

        if ($label)
            $this->label = $label;

        if ($options) {

            foreach ($options as $key => $value) {

                if (is_array($value) && $this->options[$key]) {
                    array_push($this->options[$key], $value);

                } else {
                    $this->options[$key] = $value;

                }
            }
        }

        $this->options['placeholder'] = $this->label;

    }

    protected function _init()
    {
        $this->name = $this::$defaultName;
        $this->id = $this::$defaultId;
        $this->label = $this::$defaultLabel;
        $this->options = $this::$defaultOptions;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.input');
    }
}
