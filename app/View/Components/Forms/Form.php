<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Form extends Component
{
    public $name;
    public $id;
    public $options;

    protected static $defaultName = "form";
    protected static $defaultId = "_form";
    protected static $defaultOptions = [
        'class' => null,
        'style' => null,
        'has_checkbox' => false,
        'button' => null,
        'checkbox' => null,
        'childs' => null
    ];


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = null, $id = null, $options = null)
    {
        $this->_init();

        if ($name)
            $this->name = $name;

        if ($id)
            $this->id = $id;

        if ($options) {

            foreach ($options as $key => $value) {

                if ($value) {

                    if (is_array($value) && $this->options[$key]) {
                        array_push($this->options[$key], $value);

                    } else {
                        $this->options[$key] = $value;

                    }

                }
            }
        }

    }

    protected function _init()
    {
        $this->name = $this::$defaultName;
        $this->id = $this::$defaultId;
        $this->options = $this::$defaultOptions;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.forms.form');
    }
}
