<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Card extends Component
{
    public $type;
    public $isOutlined;
    public $header = array(
        'class' => [
            'card-header',
        ],
        'style' => null,
    );

    public $body = array(
        'class' => [
            'card-body',
        ],
        'style' => null,
    );

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type = null, $isOutlined = null, $header = null, $body = null)
    {
        $this->type = $type;
        $this->isOutlined = $isOutlined;

        if ($header)
            $this->header = $this->_init($this->header, $header);

        if ($body)
            $this->header = $this->_init($this->body, $body);



        //$this->header['style'] = array_merge($this->header, $header['style']);
        //$this->body['class'] = array_merge($this->body, $body['class']);
        //$this->body['style'] = array_merge($this->body, $body['style']);

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.card');
    }

    protected function _init($arr1, $arr2)
    {
        foreach ($arr2 as $key => $value) {
            $arr1[$key] = array_merge($arr1[$key], $arr2[$key]);
            $arr1[$key] = implode (" ", $arr1[$key]);
        }

        return $arr1;
    }
}
