<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\AuthController;
use App\Models\Auth\RegisterModel;

class RegisterController extends AuthController
{
    public function __construct()
    {
        parent::__construct();

        view()->share('name', 'register');
    }

    public function index()
    {
        $model = new RegisterModel();
        $model->form__construct();

        $options = [
            'has_checkbox' => true,
            'button' => [
                'id' => '_submit',
                'name' => 'Register',
                'options' => null,
            ],
            'checkbox' => [
                'id' => 'agreeTerms',
                'name' => 'Agree Terms',
                'label' => 'I agree to the <a href="#">terms</a>',
                'options' => null,
            ],
            'childs' => $model->arr
        ];

        return view('auth.register.index', [
            'options' => $options,
        ]);
    }
}
