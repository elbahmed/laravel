<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        view()->share('layout', 'layouts.guest');
    }

}
