<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Auth\AuthController;
use App\Models\Auth\LoginModel;

class LoginController extends AuthController
{
    public function __construct()
    {
        parent::__construct();

        view()->share('name', 'login');
    }

    public function index($type)
    {
        $model = new LoginModel();
        if (!strcmp($type, 'email'))
            $model->formWithEmail__construct();

        if (!strcmp($type, 'phone'))
            $model->formWithPhone__construct();

        if (!strcmp($type, 'user_name'))
            $model->formWithUserName__construct();

        $options = [
            'has_checkbox' => true,
            'button' => [
                'id' => '_submit',
                'name' => 'Login',
                'options' => null,
            ],
            'checkbox' => [
                'id' => 'keep_login',
                'name' => 'keep_login',
                'label' => 'Keep me logged in',
                'options' => null,
            ],
            'childs' => $model->arr
        ];

        return view('auth.login.index', [
            'options' => $options,
            'type' => $type,
        ]);
    }
}
