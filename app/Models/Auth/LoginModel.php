<?php

namespace App\Models\Auth;

class LoginModel extends AuthModel
{
    public $arr;

    protected static $withEmailItem = [
        'email',
        'password',
    ];

    protected static $withPhoneItem = [
        'phone',
        'password',
    ];

    protected static $withUserNameItem = [
        'user_name',
        'password',
    ];

    protected static $type = [
        'email' => 'input',
        'phone' => 'input',
        'user_name' => 'input',
        'password' => 'input',
    ];

    protected static $label = [
        'email' => 'Email',
        'phone' => 'Phone',
        'user_name' => 'User Name',
        'password' => 'Password',
    ];

    protected static $options = [
        'email' => [
            'type' => 'email',
            'show_label' => false,
            'required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-envelope',
            ]
        ],
        'phone' => [
            'type' => 'text',
            'show_label' => false,
            'required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-phone',
            ]
        ],
        'user_name' => [
            'type' => 'text',
            'show_label' => false,
            'required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-user',
            ]
        ],
        'password' => [
            'type' => 'password',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-lock',
            ]
        ],
    ];

    public function formWithEmail__construct()
    {
        foreach ($this::$withEmailItem as $value) {
            $this->arr[$value] = [
                'id' => $value,
                'name' => $value,
                'type' => $this::$type[$value],
                'label' => $this::$label[$value],
                'options' => $this::$options[$value],
            ];
        }
    }

    public function formWithPhone__construct()
    {
        foreach ($this::$withPhoneItem as $value) {
            $this->arr[$value] = [
                'id' => $value,
                'name' => $value,
                'type' => $this::$type[$value],
                'label' => $this::$label[$value],
                'options' => $this::$options[$value],
            ];
        }
    }

    public function formWithUserName__construct()
    {
        foreach ($this::$withUserNameItem as $value) {
            $this->arr[$value] = [
                'id' => $value,
                'name' => $value,
                'type' => $this::$type[$value],
                'label' => $this::$label[$value],
                'options' => $this::$options[$value],
            ];
        }
    }
}
