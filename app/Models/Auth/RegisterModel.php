<?php

namespace App\Models\Auth;

class RegisterModel extends AuthModel
{
    public $arr;

    protected static $item = [
        'user_name',
        'email',
        'email_confirm',
        'phone',
        'first_name',
        'last_name',
        'password',
        'password_confirm',
    ];

    protected static $type = [
        'user_name' => 'input',
        'email' => 'input',
        'email_confirm' => 'input',
        'phone' => 'input',
        'first_name' => 'input',
        'last_name' => 'input',
        'password' => 'input',
        'password_confirm' => 'input',
    ];

    protected static $label = [
        'user_name' => 'User Name',
        'email' => 'Email',
        'email_confirm' => 'Email Confirm',
        'phone' => 'Phone',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'password' => 'Password',
        'password_confirm' => 'Password Confirm',
    ];

    protected static $options = [
        'user_name' => [
            'type' => 'text',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-user',
            ]
        ],
        'email' => [
            'type' => 'email',
            'show_label' => false,
            'required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-envelope',
            ]
        ],
        'email_confirm' => [
            'type' => 'email',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-envelope',
            ]
        ],
        'phone' => [
            'type' => 'text',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-phone',
            ]
        ],
        'first_name' => [
            'type' => 'text',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-user',
            ]
        ],
        'last_name' => [
            'type' => 'text',
            'show_label' => false,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-user',
            ]
        ],
        'password' => [
            'type' => 'password',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-lock',
            ]
        ],
        'password_confirm' => [
            'type' => 'password',
            'show_label' => false,
            'is_required' => true,
            'has_addon' => true,
            'addon' => [
                'icon' => 'fa-lock',
            ]
        ],
    ];

    public function form__construct()
    {
        foreach ($this::$item as $value) {
            $this->arr[$value] = [
                'id' => $value,
                'name' => $value,
                'type' => $this::$type[$value],
                'label' => $this::$label[$value],
                'options' => $this::$options[$value],
            ];
        }
    }
}
