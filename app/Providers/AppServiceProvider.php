<?php

namespace App\Providers;

use App\View\Components\Card;
use App\View\Components\Forms\Button;
use App\View\Components\Forms\Checkbox;
use App\View\Components\Forms\Form;
use App\View\Components\Forms\Input;
use App\View\Components\Forms\Label;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('card', Card::class);
        Blade::component('form-form', Form::class);
        Blade::component('form-input', Input::class);
        Blade::component('form-label', Label::class);
        Blade::component('form-button', Button::class);
        Blade::component('form-checkbox', Checkbox::class);
    }
}
