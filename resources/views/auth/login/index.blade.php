@extends($layout)

@section('title', Str::title($name))

@section('content')

<x-card type="primary" isOutlined="true" :header="array('class' => ['text-center'])">

    <x-slot name="card_header">
        <a href="/" class="h1">{{Str::remove('SYS', config('app.name'))}}<b>{{Str::remove('Bayanat', config('app.name'))}}</b></a>
    </x-slot>

    <x-slot name="card_body">

        <p class="login-box-msg">Log in to start your session</p>

        <x-form-form name="login" :options="$options"/>

        <a href="/register" class="text-center">Register a new membership</a>

        @if (!strcmp($type, 'email'))
            <div>
                <a href="/login/phone" class="d-block ">Login with phone number</a>
                <a href="/login/user_name" class="d-block ">Login with user name</a>
            </div>
        @elseif (!strcmp($type, 'phone'))
            <div>
                <a href="/login/email" class="d-block ">Login with email</a>
                <a href="/login/user_name" class="d-block ">Login with user name</a>
            </div>
        @elseif (!strcmp($type, 'user_name'))
            <div>
                <a href="/login/phone" class="d-block ">Login with phone number</a>
                <a href="/login/email" class="d-block ">Login with email</a>
            </div>
        @endif

    </x-slot>

</x-card>


@include('components.modal')


@stop

@section('style')

@stop

@section('script')
