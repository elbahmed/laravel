<div class="card {{$isOutlined ? "card-outline" : ""}} card-{{$type}}">

    <div class="{{$header['class']}}">
        {{$card_header}}
    </div>

    <div class="card-body">
        {{$card_body}}
    </div>
</div>
