<form class="form">
    <div class="row">

        @foreach ($rules['values'] as $value)
            <div class="input-group mb-3">
                <input name="<?= $value['name'] ?>" id="<?= $value['id'] ? $value['id'] : $value['name'] ?>" type="<?= $value['type'] ?>" class="form-control" placeholder="<?= $value['name'] ?>" <?= $value['required'] ? "required" : "" ?>>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas <?= $value['icon'] ? $value['icon'] : 'fa-circle' ?>"></span>
                    </div>
                </div>
            </div>
        @endforeach

        @if ($rules['config']['has_terms'])
            <div class="col-8">
                <div class="icheck-primary">
                    <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                    <label for="agreeTerms">
                    I agree to the <a href="#">terms</a>
                    </label>
                </div>
            </div>
        @endif

        <div class="col-4">
            <button class="btn btn-primary btn-block submit">{{$rules['config']['button']}}</button>
        </div>
    </div>
</form>
