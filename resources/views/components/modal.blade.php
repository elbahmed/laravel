<div class="modal fade" id="alert _modal" tabindex="-1" role="dialog" aria-labelledby="_modal_title"
	style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<h5 class="modal-title text-light" id="_modal_title"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div id="_modal_body" class="modal-body">
				<p id="_modal_description" class="description"></p>
			</div>
		</div>
	</div>
</div>
