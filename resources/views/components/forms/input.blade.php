
@if ($options['show_label'] && $label)
    <x-form-label :id="'_label'.$id" :name="$label" :for="$id" />
@endif


@if ($options['has_addon'])

<div class="input-group mb-3">
    <input name="{{$name}}" id="{{ $id ? $id : $name }}" type="{{ $options['type'] }}" class="{{$options['class'] ? implode(" ", $options['class']) : ""}}"  placeholder="{{ Str::title($options['placeholder'] ? $options['placeholder'] : $label) }}" {{array_key_exists('value', $options) ? "value=".$options['value'] : ""}} {{ $options['required'] ? "required" : "" }} {{ $options['disabled'] ? "disabled" : "" }} {{ $options['readonly'] ? "readonly" : "" }}>

    <div class="input-group-append">
        <div class="input-group-text">
            <span class="fas {{ $options['addon']['icon'] ? $options['addon']['icon'] : 'fa-circle' }}"></span>
        </div>
    </div>
</div>

@else

<input name="{{$name}}" id="{{ $id ? $id : $name }}" type="{{ $options['type'] }}" class="{{$options['class'] ? implode(" ", $options['class']) : ""}}"  placeholder="{{ Str::title($options['placeholder'] ? $options['placeholder'] : $label) }}" {{ $options['required'] ? "required" : "" }}>

@endif
