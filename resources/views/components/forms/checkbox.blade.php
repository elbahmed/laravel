<div class="{{implode(" ", $options['class'])}}">
    <input type="checkbox" id="{{$id}}" name="{{$name}}" value="{{$name}}">
    <label for="{{$id}}">
        {!! Str::ucfirst($label) !!}
    </label>
</div>
