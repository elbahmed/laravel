<button id="{{$id}}" class="{{$options['class'] ? implode(" ", $options['class']) : ""}}" style="{{$options['style'] ? implode(" ", $options['style']) : ""}}">{{Str::title($name)}}</button>
