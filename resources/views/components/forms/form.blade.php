<form id="{{$id}}" class="{{$options['class'] ? implode(" ", $options['class']) : ""}}">
    <div class="row">

        @foreach ($options['childs'] as $value)

            <div class="form-group col-12">

                @if (!strcmp($value['type'], 'input'))

                    <x-form-input :id="$value['id']" :name="$value['name']" :label="$value['label']" :options="$value['options']"/>

                @elseif (!strcmp($value['type'], 'select'))

                @endif
            </div>

        @endforeach

        @if ($options['has_checkbox'])

            <div class="col-8">
                <x-form-checkbox :id="$options['checkbox']['id']" :name="$options['checkbox']['name']" :label="$options['checkbox']['label']" :options="$options['checkbox']['options']"/>
            </div>

        @endif

        <div class="col-4">
            <x-form-button :id="$options['button']['id']" :name="$options['button']['name']" :options="$options['button']['options']"/>
        </div>

    </div>
</form>
