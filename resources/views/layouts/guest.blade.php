<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>@yield('title') | {{config('app.name')}}</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('storage/themes/img/favicon.ico')}}">

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
		<link rel="stylesheet" href="{{ asset('storage/themes/plugins/fontawesome-free/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('storage/themes/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('storage/themes/css/adminlte.min.css')}}">

		@yield('style')

	</head>
	<body class="{{$name}}-page">

		<div class="{{$name}}-box">

            @yield('content')

		</div>

		<script src="{{ asset('storage/themes/plugins/jquery/jquery.min.js')}}"></script>
		<script src="{{ asset('storage/themes/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{ asset('storage/themes/js/adminlte.min.js')}}"></script>

		@yield('script')

	</body>
</html>
